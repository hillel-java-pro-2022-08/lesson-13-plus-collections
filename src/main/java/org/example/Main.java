package org.example;

import org.example.collections.MyHashMap;
import org.example.collections.MyHashSet;
import org.example.collections.MyLinkedList;
import org.example.compare.Student;
import org.example.compare.StudentByNameComparator;
import org.example.io.IOStart;
import org.example.stream.StreamExamples;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

    private static void listsExample() {
        List<String> l = new MyLinkedList<>();
        l.add("one");
        l.add("two");
        l.add("three");
        l.add("two");


//
//        for (String s : l) {
//            System.out.println(s);
//        }

        // O(1) = 1,2....
        // O(n) = for(0..n)
        // O(n^2) = for(){for(){}}   <--


        Student a = new Student("Vasia", "Pupkin", 1);
        Student b = new Student("Petia", "Vasichkin", 2);
//coparable
        if (a.compareTo(b) > 0) {
            System.out.println("student a > b");
        } else {
            System.out.println("student a <= b");
        }
//comparator

        Comparator<Student> studentComparator = Student.byCourseComparator();
        if (studentComparator.compare(a, b) > 0) {
            System.out.println("*student a > b");
        } else {
            System.out.println("*student a <= b");
        }


        Object[] elems = l.toArray();
        String[] elemsS = l.toArray(new String[0]);
    }

    private static void setExamples() {
        Set<String> mySet = new MyHashSet<>();
        mySet.add("one");
        mySet.add("two");
        mySet.add("one");
        mySet.add("three");
        mySet.add("two");
        mySet.add("four");


        MyHashSet<String> x = new MyHashSet<>();
        x.add("one");
        x.add("two");
        x.add("five");
        mySet.containsAll(x);//false

        for (String s : mySet) {
            System.out.println(s);
        }
    }

    private static void mapExamples() {
        Map<String, String> footMap = new HashMap<>();
        footMap.put("Apple", "fruit");
        footMap.put("Banana", "fruit");
        footMap.put("Cucumber", "vegetable");

        System.out.println(footMap.get("Apple"));


        Map<Integer, String> days = new MyHashMap<>();
        days.put(1, "Monday");
        days.put(2, "Tuesday");
        System.out.println(days.get(2));

        if (days.containsKey(2)) {
            //....
            System.out.println("contains day 2");
        }

        Set<Map.Entry<Integer, String>> entries = days.entrySet();
        for (Map.Entry<Integer, String> entry : entries) {
            System.out.printf("%d => %s\n", entry.getKey(), entry.getValue());
        }


    }

    public static void main(String[] args) {
        //listsExample();
        //setExamples();
        // mapExamples();
       //queueExample();
        //streamExample();

//        StreamExamples streamExamples = new StreamExamples();
//        streamExamples.run();

        IOStart ioStart = new IOStart();
        ioStart.run();


    }

    private static void streamExample() {
        String strings1 = getStringStream()
                .collect(Collectors.joining(","));


        System.out.println(strings1);

    }

    private static Stream<String> getStringStream() {
        List<String> strings = List.of("one","two","three");

        return strings.stream()
                .filter(e->!e.equals("two"))
                .map(e->e.toUpperCase(Locale.ROOT));
    }

    private static void queueExample() {

        Deque<String> queue = new LinkedList<>();
        queue.addFirst("one");
        queue.addFirst("two");

        while (!queue.isEmpty()){
            System.out.println(queue.pollFirst());
        }



//        System.out.println(queue.poll());
//        System.out.println(queue.poll());
//        System.out.println(queue.poll());


 //       System.out.println(queue.element());
  //      System.out.println(queue.element());

  //      System.out.println(queue.peek());
//        System.out.println(queue.peek());


    }

}
