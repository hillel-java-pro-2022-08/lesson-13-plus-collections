package org.example.collections;

import java.util.*;

public class MyLinkedList<T> implements List<T> {

    private static class Node<T> {
        T value;
        Node<T> next;
        Node<T> prev;

        public Node(T value) {
            this.value = value;
        }
    }

    private Node<T> head;
    private Node<T> tail;
    private int size;


    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return head == null;
    }

    @Override
    public boolean contains(Object o) {
        for (T t : this) {
            if(Objects.equals(t,o)) return true;
        }
        return false;
    }

    @Override
    public Iterator<T> iterator() {
        return new MyLinkedListIterator();
    }

    @Override
    public Object[] toArray() {
        return toArray(new Object[0]);
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        T1[] result = Arrays.copyOf(a, size);
        int i = 0;
        for (T elem : this) {
            result[i++] = (T1) elem;
        }
        return result;
    }

    @Override
    public boolean add(T elem) {
        Node<T> node = new Node<T>(elem);
        if (isEmpty()) {
            head = tail = node;
        } else {
            tail.next = node;
            node.prev = tail;
            tail = node;
        }
        size++;
        return true;
    }

    @Override
    public boolean remove(Object o) {
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        for (Object o : c) {
            if (!contains(o)) return false;
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        return false;
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        Iterator<T> it = iterator();
        while (it.hasNext()){
            if(c.contains(it.next())){
                it.remove();
            }
        }
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        Iterator<T> it = iterator();
        while (it.hasNext()){
            if(!c.contains(it.next())){
                it.remove();
            }
        }
        return false;
    }

    @Override
    public void clear() {

    }

    @Override
    public T get(int index) {
        Node<T> node = getNode(index);
        return node.value;
    }

    @Override
    public T set(int index, T element) {
        Node<T> node = getNode(index);
        node.value = element;
        return element;
    }

    @Override
    public void add(int index, T element) {

    }

    @Override
    public T remove(int index) {
        Node<T> node = getNode(index);
        removeNode(node);
        return node.value;
    }

    @Override
    public int indexOf(Object o) {
        return 0;
    }

    @Override
    public int lastIndexOf(Object o) {
        return 0;
    }

    @Override
    public ListIterator<T> listIterator() {
        return null; //do not do it
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        return null; //do not do it
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        //check indexes
        MyLinkedList<T> list = new MyLinkedList<>();

        Node<T> cur = getNode(fromIndex);
        for (int i = fromIndex; i < toIndex; i++) {
            list.add(cur.value);
            cur = cur.next;
        }
//
//        int i = 0;
//        for (T elem : this) {
//            if (i < fromIndex) continue;
//            if (i >= toIndex) break;
//            list.add(elem);
//            i++;
//        }

        return list;
    }

    private class MyLinkedListIterator implements Iterator<T> {
        private Node<T> cur;

        public MyLinkedListIterator() {
            cur = new Node<>(null);
            cur.next = head;
        }

        @Override
        public boolean hasNext() {
            return cur.next != null;
        }

        @Override
        public T next() {
            cur = cur.next;
            T value = cur.value;
            return value;
        }

        @Override
        public void remove() {
            removeNode(cur);
        }
    }

    private void removeNode(Node<T> cur) {
        if (cur.prev == null) {
            head = cur.next;
        }else{
            cur.prev.next = cur.next;
        }
        if (cur.next == null) {
            tail = cur.prev;
        }else{
            cur.next.prev = cur.prev;
        }
        size--;
    }

    private Node<T> getNode(int index) {

        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException(index);
        }

        Node<T> node = head;
        for (int i = 0; i < index; i++) {
            node = node.next;
        }
        return node;
    }
}
