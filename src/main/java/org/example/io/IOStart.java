package org.example.io;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.example.stream.Human;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.naming.BinaryRefAddr;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;

public class IOStart {

    private static final String PARRENT_PATH = "./";
    private static final String CHILD_PATH = "pom.xml";


    private void fileInfoExample() {
        File file = new File(PARRENT_PATH, CHILD_PATH);

        System.out.println(file.getPath());
        System.out.println(file.getAbsolutePath());

        if (file.exists()) {
            System.out.println("pom.xml exists");
        }

        //file.getAbsolutePath();
        //file.getAbsoluteFile();

        //file.canWrite();
        //file.canRead();
        //file.canExecute();
        //file.isDirectory();
        //file.isFile();
        //file.isAbsolute()


    }

    private void scanFolderExample() {
        File dir = new File("./");
        if (dir.isDirectory()) {
            File[] files = dir.listFiles();
            String[] paths = dir.list();

            for (String path : paths) {
                System.out.println(path);
            }

            List<String> folders = Arrays.stream(files)
                    .filter(File::isDirectory)
                    .filter(file -> !file.isHidden())
                    .map(File::getName)
                    .map(path -> "[DIR] " + path)
                    .collect(Collectors.toList());

            System.out.println(folders);


            List<String> folders2 = new ArrayList<>();
            for (File file : files) {
                if (!file.isDirectory()) {
                    continue;
                }
                if (file.isHidden()) {
                    continue;
                }
                String name = "[DIR] " + file.getPath();
                folders2.add(name);
            }

            System.out.println(folders2);
        }
    }

    void readFileExample() {
        try (BufferedInputStream inputStream = new BufferedInputStream(new FileInputStream("pom.xml"))) {
//            byte[] bytes = inputStream.readAllBytes();
//            String content = new String(bytes);
//            System.out.println(content);

            Scanner scanner = new Scanner(inputStream);
            int i=0;
            while (scanner.hasNextLine()){
                System.out.println(i++ + " : "+scanner.nextLine());
            }

        } catch (IOException exception) {
            //....
        }
    }

    void readFileReaderExample() {
        try (BufferedReader reader = new BufferedReader(new FileReader("pom.xml"))) {
            int i=0;
            while (true){
                String s = reader.readLine();
                if(s==null) break;
                System.out.println(i++ + " : "+ s);
            }

        } catch (IOException exception) {
            //....
        }
    }

    void writeFileExample(String content) {
        try (BufferedOutputStream os = new BufferedOutputStream(new FileOutputStream(
                "1.txt", true
        ))) {
            os.write(content.getBytes(StandardCharsets.UTF_8));
            os.flush();
        } catch (IOException exception) {
            //....
        }
    }

    void writeFileExampleWriterString(String content) {
        try (BufferedWriter os = new BufferedWriter(new FileWriter(
                "1.txt", true
        ))) {
            os.write(content);
            os.flush();
        } catch (IOException exception) {
            //....
        }
    }

    void rwObject(){
        Human h = new Human("vasia","pupkin",123);
        Human h2 = new Human("vasia2","pupkin2",125);

        try(ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream("humans.obj"))){
            outputStream.writeInt(2);
            outputStream.writeObject(h);
            outputStream.writeObject(h2);
            outputStream.flush();
        } catch (IOException exception) {
            exception.printStackTrace();
        }

        try(ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("humans.obj"))){
           int count = objectInputStream.readInt();
            for (int i = 0; i < count; i++) {
                System.out.println(objectInputStream.readObject());
            }
        } catch (IOException | ClassNotFoundException exception) {
            exception.printStackTrace();
        }
    }

    private void gsonExample(){
        List<Human> humans = new ArrayList<>();
        humans.add(new Human("Vasia","Pupkin",1990));
        humans.add(new Human("Vasia","Pupkin",1990));
        humans.add(new Human("Vasia","Pupkin",1990));

        Gson gson = new Gson();

        String json = gson.toJson(humans);
        System.out.println(json);

        var ls = gson.fromJson(json, new TypeToken<List<Human>>(){});
        System.out.println(ls);
    }

    private void jacksonExample(){
        List<Human> humans = new ArrayList<>();
        humans.add(new Human("Vasia","Pupkin",1990));
        humans.add(new Human("Vasia","Pupkin",1990));
        humans.add(new Human("Vasia","Pupkin",1990));

        ObjectMapper objectMapper = new ObjectMapper();
        ObjectMapper objectMapper2 = new XmlMapper();
        objectMapper.setDefaultPropertyInclusion(JsonInclude.Include.ALWAYS);

        try {
            String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(humans);
            System.out.println(json);

            System.out.println(//XML
                    objectMapper2
                            .writerWithDefaultPrettyPrinter()
                            .writeValueAsString(humans)
            );

            var ls = objectMapper.readValue(json, new TypeReference<List<Human>>() {});
            System.out.println(ls);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }


    public void run() {
        //fileInfoExample();
        //scanFolderExample();
        //readFileExample();
        //readFileReaderExample();
        //rwObject();


        //writeFileExample("Hello world!");
       // writeFileExampleWriterString("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");

        //gsonExample();
        jacksonExample();


    }
}
