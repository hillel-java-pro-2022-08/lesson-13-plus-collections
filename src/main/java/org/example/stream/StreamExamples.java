package org.example.stream;

import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StreamExamples {


    Stream<Human> streamCreation() {
        Stream<Human> stream1 = Stream.of(
                new Human("Vasia", "Pupkin", 1990),
                new Human("Petia", "Vasichkin", 1991),
                new Human("Ivan", "Ivanov", 1989),
                new Human("Test", "Testov", 2001)
        );


        // Arrays.stream(new Integer[]{1,2,3});

//        Stream<Human> stream2 = Set.of(
//                new Human("Vasia", "Pupkin", 1990),
//                new Human("Petia", "Vasichkin", 1991),
//                new Human("Ivan", "Ivanov", 1989)
//        ).stream();

        return stream1;
    }


    public void filterAndMapExample() {

        streamCreation()
                .filter(new Predicate<Human>() {
                    @Override
                    public boolean test(Human human) {
                        return human.getBornYear() <= 2000;
                    }
                })
                .map(human -> human.getName() + " " + human.getSurname())
                .forEach(System.out::println);


    }

    public void collectExample() {
        Set<String> surnames = streamCreation()
                .map(Human::getSurname)
                .collect(Collectors.toCollection(HashSet::new));

        List<String> names = streamCreation()
                .map(h -> h.getSurname())
                .collect(Collectors.toList());

        String names2 = streamCreation()
                .map(Human::getSurname)
                .collect(Collectors.joining(", "));

//        List<Goods> goods =  categories.stream()
//                        .flatMap(cat->cat.goods().stream())
//                .filter(goods->goods.getP)
//                                .collect(Collectors.toList());


        surnames.add("TestAdd");

        for (String surname : surnames) {
            System.out.println(surname);
        }

        System.out.println(names2);
    }

    public void matchesExample() {
        if (streamCreation().allMatch(h -> h.getBornYear() < 2000)) {
            System.out.println("All of them have born year < 2000");
        }

        if (streamCreation().anyMatch(h -> h.getBornYear() < 2000)) {
            System.out.println("have a people with born year < 2000");
        }

        if (streamCreation().noneMatch(h -> h.getBornYear() > 2020)) {
            System.out.println("noneMatch");
        }
    }

    public void simpleExample() {
        System.out.println(streamCreation().count());
        System.out.println(streamCreation().min(Comparator.comparingInt(Human::getBornYear)));
        System.out.println(streamCreation().max(Comparator.comparing(Human::getSurname)));
    }


    public void optionalExample() {
//        Optional<Human> hO = Optional.of(new Human("A", "B", 3));
////        Optional.ofNullable(new Human("A","B",3));
////        Optional.empty();// Optional.ofNullable(null);
//
//        if (hO.isEmpty()) {
//            System.out.println("No humans");
//        }
//
//        if (hO.isPresent()) {
//            System.out.println("Exist human " + hO.get());
//        }
//
//
//
//
//
//
//        String name = hO
//                .filter(h -> h.getBornYear() < 0)
//                .map(h -> h.getName())
//                .orElseThrow(()->new IllegalArgumentException());
//
//        System.out.println(name);


        Integer bornYearOfPupkin = streamCreation()
                .filter(h -> h.getSurname().equals("Pupkin"))
                .findFirst()
                //.filter(h->!h.getName().equals("Vasia"))
                .map(h -> h.getBornYear())
                .orElse(null);

        System.out.println(bornYearOfPupkin);


    }

    private void groupingExample() {
        Map<String, List<Human>> result = Stream.of(
                new Human("Vasia", "Pupkin", 1),
                new Human("Petia", "Pupkin", 2),
                new Human("Ivan", "Ivanov", 3),
                new Human("Vasia", "Sidorov", 4),
                new Human("Vasia", "Ivanov", 5)
        ).collect(Collectors.groupingBy(Human::getSurname));

        System.out.println(result.get("Pupkin"));
        System.out.println(result.get("Ivanov"));
    }

    public void toMapExample(){
        Map<String,Good> goodMap = Stream.of(
                new Good("PH001","Mobile phone 1",100500),
                new Good("PH002","Mobile phone 2",100500),
                new Good("PH003","Mobile phone 3",100500),
                new Good("PH004","Mobile phone 4",100500),
                new Good("V001","Vacuum cleaner 1",100500),
                new Good("V002","Vacuum cleaner 2",100500),
                new Good("V003","Vacuum cleaner 3",100500),
                new Good("V004","Vacuum cleaner 4",100500),
                new Good("V005","Vacuum cleaner 5",100500)
        ).collect(Collectors.toMap(Good::getId, good->good));

        System.out.println(goodMap.get("PH004"));
    }


    public void run() {
        // filterAndMapExample();
        //collectExample();
        //matchesExample();
        //simpleExample();

        //optionalExample();

        //groupingExample();
        //toMapExample();
    }
}
