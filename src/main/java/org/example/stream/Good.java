package org.example.stream;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Good {
    private String id;
    private String name;
    private int price;
}
