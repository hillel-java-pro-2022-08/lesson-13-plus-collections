package org.example.compare;

import java.util.Comparator;

public class Student implements Comparable<Student> {

    public Student(String name, String surname, int course) {
        this.name = name;
        this.surname = surname;
        this.course = course;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getCourse() {
        return course;
    }

    public void setCourse(int course) {
        this.course = course;
    }

    String name;
    String surname;
    int course;

    @Override
    public int compareTo(Student o) {
        int surnameComp = surname.compareTo(o.surname);
        if (surnameComp != 0) return surnameComp;
        return name.compareTo(o.name);
    }

    public static Comparator<Student> byNameComparator() {
        return Comparator
                .comparing(Student::getName)
                .thenComparing(Student::getSurname);
    }

    public static Comparator<Student> byCourseComparator() {
        return (s1, s2) -> s1.getCourse() - s2.getCourse();
    }
}
