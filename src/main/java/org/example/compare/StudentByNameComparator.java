package org.example.compare;

import java.util.Comparator;

public class StudentByNameComparator implements Comparator<Student> {
    @Override
    public int compare(Student o1, Student o2) {
        int nameComp = o1.getName().compareTo(o2.getName());
        if (nameComp != 0) return nameComp;
        return o1.getSurname().compareTo(o2.getSurname());
    }
}
